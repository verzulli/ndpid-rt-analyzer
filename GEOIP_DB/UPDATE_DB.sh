#!/bin/bash

########
# Remember that you need to define your license_key to update Database
# eg.: export LK=hg**********KB
#

if [ -z "$LK" ]
then
    echo "Error!"
    echo "You need to setup an LK environment variable with related license key!"
    echo "If you already have the license key, you simply need to re-run this script"
    echo "but, before, you need an:"
    echo ""
    echo "$ export LK=<your_key_string>"
    echo ""
    echo "If you DON'T have a license key, you need to apply for one. Plese check:"
    echo "https://support.maxmind.com/hc/en-us/articles/4407111582235-Generate-a-License-Key"
else 
    echo "License key is defined! Let's proceed..."

    # Database URL
    wget -O GeoLite2-ASN.tar.gz "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-ASN&license_key=${LK}&suffix=tar.gz"
    # SHA256 URL
    wget -O GeoLite2-ASN.tar.gz.sha256 "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-ASN&license_key=${LK}&suffix=tar.gz.sha256"


    # Database URL
    wget -O GeoLite2-Country.tar.gz "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country&license_key=${LK}&suffix=tar.gz"
    # SHA256 URL
    wget -O GeoLite2-Country.tar.gz.sha256 "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country&license_key=${LK}&suffix=tar.gz.sha256"

    echo "Done!"
    echo "Now you need to MANUALLY extract 'GeoLite2-ASN.mmdb' and 'GeoLite2-Country.mmdb' from above tar.gz"
    echo "and put them here"
fi

