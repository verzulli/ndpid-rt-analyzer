import * as dotenv from 'dotenv'
import logger from './components/logger.js'
import engineConsole from './components/engineConsole'
import injestor from './components/injestor'
import receiver from './components/receiver'
import apiServer from './components/apiServer'
import websocketServer from './components/websocketServices'
import geoResolver from './components/geoResolver'

// initialize .env file management
dotenv.config()

logger.info('[MAIN] Starting Console server')
engineConsole.startConsoleServer()

logger.info('[MAIN] Initializing GeoResolver component')
geoResolver.init()

logger.info('[MAIN] Initializing injestor')
injestor.init()

logger.info('[MAIN] Starting API server')
apiServer.start()

logger.info('[MAIN] Starting WEBSOCKET Services')
websocketServer.start()

process.on('SIGTERM', async () => {
  logger.info('[MAIN] received SIG-TERM. Terminating....')
  receiver.terminate()
  apiServer.terminate()
  await websocketServer.terminate()
  engineConsole.terminate()
})

process.on('SIGINT', async () => {
  logger.info('[MAIN] received SIG-INT. Terminating....')
  receiver.terminate()
  apiServer.terminate()
  await websocketServer.terminate()
  engineConsole.terminate()
})

logger.info('[MAIN] All done!')
