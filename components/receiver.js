import logger from '../components/logger'
import dgram from 'dgram'
import net from 'net'
import readline from 'readline'
import parser from '../components/parser'
import { appendFileSync } from 'node:fs'

const _counters = {
  'in': 0, // globally received
  'ok': 0, // size and JSON => ok
  'err': 0, // size mismatch
  'evt': {
    'flows': 0,
    'pkt': 0,
    'others': 0
  }
}

let _jsonDumper = false

// UDP socket handler
let _UDPserver = null
let _TCPclient = null
const _receiverStatus = {
  'udp': false,
  'tcp': false
}

const _startedOn = Date.now() // to keep track of starting time

export default {

  // initialize receiving UDP socket and prepare required message handle
  // This is mainly required to receive the JSON-stream coming directly
  // from nDPId...
  initUDP: function (port) {
    logger.info('[receiver/initUDP] Initializing receiving UDP socket...')

    // creating socket
    _UDPserver = dgram.createSocket({
      'type': 'udp4'
      // 'recvBufferSize': 32768
    })

    // setting error handler
    _UDPserver.on('error', (err) => {
      logger.error('[receiver/initUDP/hdlError] Server error: [' + err.stack + ']')
      _UDPserver.close()
      _receiverStatus.udp = false
    })

    // setting received UDP-messages handler
    _UDPserver.on('message', (msg, rinfo) => {
      logger.debug(`[receiver/initUDP/hdlMessage] received [${msg}] from [${rinfo.address}:${rinfo.port}]`)
      _counters.in++
      const content = this.checkFormat(msg)
      if (content) {
        if (this.preParseMessage(content)) {
          _counters.ok++
        } else {
          _counters.err++
        }
      } else {
        _counters.err++
      }
    })

    logger.info('[receiver/initUDP] Binding the UDP-server socket on port [' + port + ']')
    _UDPserver.bind(port)
    _receiverStatus.udp = true
  },

  // connect to a remote nDPIsrvd server, via TCP
  initTCP: function (host, port) {
    logger.info('[receiver/initTCP] Connecting to TCP [' + host + ':' + port + ']')

    // we're going to return a Promise, as our TCP client is _ASYNCHRONOUS_ and
    // we cannot know, _NOW_, if connection will succeed, or not.
    // Hence, we return a promise + requestor will "await" and... at proper time
    // we'll return a Reject (error) or Resolve (OK)
    return new Promise((resolve, reject) => {
      // instantiate the TCP socket connection
      _TCPclient = net.createConnection(port, host, () => {
        // 'connect' listener.
        logger.info('[receiver/initTCP/onConnect] connected to remote nDPIsrvd!')
      })

      // 'end' event => handle graceful termination/disconnections
      _TCPclient.on('end', () => {
        logger.info('[receiver/initTCP/onEnd] TCP socket disconnected')
        _receiverStatus.tcp = false
      })

      // 'error' event => handle errors
      _TCPclient.on('error', () => {
        logger.info('[receiver/initTCP/onError] TCP socket error')
        _receiverStatus.tcp = false
        reject(Error('Error connecting to client'))
      })

      // 'connect' event => handle succesfull connections
      _TCPclient.on('connect', () => {
        logger.info('[receiver/initTCP/onConnect] TCP socket connected')
        _receiverStatus.tcp = true

        // We're going to rely on "readline" to properly process TCP data on a
        // line-by-line basis (...instead of packet-by-packet)
        const rl = readline.createInterface({
          input: _TCPclient,
          output: process.stdout,
          terminal: false
        })

        rl.on('line', (msg) => {
          logger.debug(`[receiver/initTCP/onReadLineData] received [${msg}]`)
          _counters.in++
          const content = this.checkFormat(msg)
          if (content) {
            if (this.preParseMessage(content)) {
              _counters.ok++
            } else {
              _counters.err++
            }
          } else {
            _counters.err++
          }
        })

        // TCP connection have been SUCCESFULL!
        resolve()
      })
    })
  },

  // ensure that received messages are well-formatted
  // (ref: https://github.com/utoni/nDPId#json-stream-format )
  checkFormat: (msg) => {
    try {
      const size = parseInt(msg.toString().substring(0, 5))
      const content = msg.toString().substring(5)
      logger.debug(`[receiver/checkFormat] [${size}] - [` + content.length + ']')
      if (_jsonDumper) {
        appendFileSync('/tmp/raw_json.stream', content)
      }
      return content
    } catch (e) {
      logger.error(`[receiver/checkFormat] error: [${e}]`)
      return false
    }
  },

  preParseMessage: function (dataString) {
    try {
      const data = JSON.parse(dataString)
      // logger.info(`[parseFlow] Parse OK`)
      if (data.hasOwnProperty('packet_event_id')) {
        _counters.evt.pkt++

        // proceed with "packet" parsing data
        parser.parsePacket(data)
      } else if (data.hasOwnProperty('flow_event_id')) {
        _counters.evt.flows++

        // let'a add current timestamp to data
        data.arrivalTime = Date.now()

        // proceed with "flow" parsing data
        parser.parseFlow(data)
      } else {
        _counters.evt.others++
      }
      return true
    } catch (e) {
      logger.error(`[receiver/preParseMessage] error: [${e}]`)
      return false
    }
  },

  terminate: function () {
    logger.info('[receiver/terminate] terminating...')
    if (_UDPserver !== null) {
      logger.info('[receiver/terminate] Closing the _UDPserver active socket...')
      _UDPserver.close()
      _receiverStatus.udp = false
    }
    if (_TCPclient !== null) {
      logger.info('[receiver/terminate] Closing the _TCPclient connection...')
      _TCPclient.end()
      _receiverStatus.tcp = false
    }
  },

  getCounters: function () {
    return _counters
  },

  getStartedOn: function () {
    return _startedOn
  },

  getStatus: function () {
    return _receiverStatus
  },

  enableJsonDumper: function () {
    _jsonDumper = true
  },

  disableJsonDumper: function () {
    _jsonDumper = false
  }

}
