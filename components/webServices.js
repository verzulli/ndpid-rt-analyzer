import logger from './logger'
import receiver from './receiver'
import parser from './parser'

export default {

  fetchCounters: async (req, res) => {
    logger.debug('[webServices/fetchCounters] Retrieving counters')

    const aCounters = receiver.getCounters()
    const pCounters = parser.getCounters()

    res.json({
      'receiver': aCounters,
      'parser': pCounters
    })
  },

  fetchRiskMap: async (req, res) => {
    logger.info('[webServices/fetchRiskMap] Retrieving RiskMap')

    // fetch full riskmap and flowmap
    const riskMap = parser.getRiskFlowMap()
    const fullMap = parser.getCompletedFlowMap()

    // retrieve all flows involved in at least one risk
    const riskFlows = {}
    for (const r in riskMap) {
      logger.info('[webServices/fetchRiskMap] Processing risk [' + r + '] with IDs =>>' + riskMap[r].join('|') + '<==')
      riskMap[r].forEach(id => {
        riskFlows[id] = fullMap[id]
      })
    }
    res.json({
      'riskmap': riskMap,
      'flows': riskFlows
    })
  },

  fetchProtoMap: async (req, res) => {
    logger.info('[webServices/fetchProtoMap] Retrieving ProtoMap')

    // fetch full riskmap and flowmap
    const protoMap = parser.getProtoMap()
    const fullMap = parser.getCompletedFlowMap()

    // retrieve all flows involved in at least one risk
    const protos = {}
    for (const r in protoMap) {
      logger.info('[webServices/fetchRiskMap] Processing risk [' + r + '] with IDs =>>' + protoMap[r].join('|') + '<==')
      protoMap[r].forEach(id => {
        protos[id] = fullMap[id]
      })
    }
    res.json({
      'protos': protoMap,
      'flows': protos
    })
  },

  tcpConnect: async (req, res) => {
    logger.debug('[webServices/tcpConnect] got request -->' + JSON.stringify(req.body) + '<--')
    try {
      if (!(/^\d+\.\d+\.\d+\.\d+$/.test(req.body.host))) {
        res.status(400).send('Bad request: wrong/missing host!')
        return
      }
      if (!(/^\d+$/.test(req.body.port))) {
        res.status(400).send('Bad request: wrong/missing port!')
        return
      }
      const remoteHost = req.body.host
      const remotePort = req.body.port
      logger.debug('[webServices/tcpConnect] connecting to [' + remoteHost + ':' + remotePort + ']')
      await receiver.initTCP(remoteHost, remotePort)
    } catch (e) {
      logger.error('[webServices/tcpConnect] Errore nella chiamata WS [' + e + ']')
      res.status(400).send('Your request cannot be fulfilled. Ask someone for help!')
    }
    res.status(200).send()
  },

  stopReceiver: async (req, res) => {
    logger.debug('[webServices/stopReceiver] stopping receiver')
    receiver.terminate()
    res.status(200).send()
  },

  statusReceiver: async (req, res) => {
    logger.debug('[webServices/stopReceiver] stopping receiver')
    const currentStatus = receiver.getStatus()
    res.json(currentStatus)
  }

}
