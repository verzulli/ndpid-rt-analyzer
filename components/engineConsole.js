import logger from './logger'
import net from 'net'
import cliHandlers from './cliHandlers'

export default {

  curClientID: 0,
  curConsoleClientID: 0,
  serverConsolePort: null,
  serverAddress: null,
  prompt: '(no prompt)',
  clients: [],
  consoleClients: [],

  startConsoleServer () {
    const console = this

    console.serverConsolePort = process.env.CLI_CONSOLE_PORT
    console.serverAddress = process.env.CLI_CONSOLE_BIND_ADDRESS
    console.prompt = process.env.CLI_CONSOLE_PROMPT

    logger.info('[EngineConsole/startConsoleServer] Starting console server: [' + console.serverAddress + '/' + console.serverConsolePort + ']')
    console.consoleConnection = net.createServer((socket) => { console.onConsoleClientConnected(socket) })
    console.consoleConnection.listen(console.serverConsolePort, console.serverAddress)
  },
  onConsoleClientConnected (socket) {
    logger.info('[EngineConsole/onConsoleClientConnected] New client connected: [' + socket.remoteAddress + '/' + socket.remotePort + ']')
    this.curConsoleClientID++

    // Identify this client
    socket.logmonConsoleClient = {
      'id': this.curConsoleClientID,
      'name': 'Console Client: [' + socket.remoteAddress + ':' + socket.remotePort + ']'
    }

    // Put this new client in the list
    this.consoleClients.push(socket)

    // Send a nice welcome message and announce
    socket.write('****************************************************\n')
    socket.write('***            nDPId-rt-analyzer                 ***\n')
    socket.write('***         v. 0.1 - Console Server              ***\n')
    socket.write('****************************************************\n')
    socket.write('\n')
    socket.write(this.prompt + '> ')

    // Handle incoming messages from clients.
    socket.on('data', (data) => {
      this.onConsoleSocketData(socket, data)
    })

    // Remove the client from the list when it leaves
    socket.on('end', () => {
      this.onConsoleSocketEnd(socket)
    })
  },
  onConsoleSocketData (socket, inputData) {
    logger.debug('[EngineConsole/onConsoleSocketData] Got ==>' + inputData + '<=== from ' + socket.logmonConsoleClient.id + ' [' + socket.remoteAddress + '/' + socket.remotePort + ']')

    // let's get rid of leading and/or trailing spaces
    const command = String(inputData).replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '').replace(/[^A-Za-z0-9_. ]+/g, '').toLowerCase()

    logger.debug('[EngineConsole/onConsoleSocketData] Processing ==>' + command + '<===')

    // let's process the command, unless it's a "logout"
    if (command !== 'logout' && command !== 'exit' && command !== 'quit') {
      // ...and we simply ignore empty commands
      if (command !== '') {
        // ok. So we got a command (aka: something typed in the CLI). Let's see
        // if it's a valid command (aka: a command "mapped" to some action)

        // let's check if we have a "mapped methods" associated to the command
        const handlerFound = []
        for (let idx = 0; idx < cliHandlers.commandMap.length; idx++) {
          const k = cliHandlers.commandMap[idx]
          if (command.search(k.regexp) !== -1) {
            handlerFound.push(k)
          }
        }

        // Have we found a "matching" handler?
        if (Array.isArray(handlerFound) && handlerFound.length > 0) {
          // yes. We found something...

          if (handlerFound.length === 1) {
            // We found exactly 1 handler! Let's de-reference the related routine
            logger.debug('[EngineConsole/onConsoleSocketData] Found handler for ==>' + command + '<===')

            // here is our "stanza" with information regarding what we have to do..
            const handler = handlerFound[0]

            // and how... let's "call" it, passing "socket" as the argument
            logger.info('[EngineConsole/onConsoleSocketData] Executing ==>' + handler.handler + '<===')
            cliHandlers[handler.handler](socket, command)
          } else {
            // we found more than one handler. Let's tell the user...
            logger.info('[EngineConsole/onConsoleSocketData] Too many handlers found ==>' + command + '<===')
            cliHandlers.ambiguousCommand(socket)
          }
        } else {
          // we haven't found an handler for what has been typed to che CLI
          // let's tell the user...
          logger.info('[EngineConsole/onConsoleSocketData] Handler NOT found for ==>' + command + '<===')
          cliHandlers.unknownCommand(socket)
        }
        socket.write('\n')
      }
      socket.write(this.prompt + '> ')
    } else {
      // let's close the socket
      socket.end()
    }
  },
  onConsoleSocketEnd (socket) {
    logger.info('[EngineConsole/onConsoleSocketEnd] Client [' + socket.remoteAddress + '/' + socket.remotePort + '] disconnected!')
    this.consoleClients.splice(this.consoleClients.indexOf(socket), 1)
  },

  terminate () {
    const console = this
    logger.info('[EngineConsole/terminate] Closing console server')
    console.consoleConnection.close()
  }
}
