import logger from '../components/logger'
import { sprintf } from 'sprintf-js'
import injestor from '../components/injestor'
import geoResolver from '../components/geoResolver'

const _counters = {
  'flows': {},
  'packets': {},
  'risks': {},
  'protos': {}
}

// counter for detected flows
let _ctrDetected = 0

// main Global Flow Map
const _flowMap = {} // global
let _completedFlowMap = {} // terminated flows
const _riskFlowMap = {} // "risky flows" map { risk => Array of related risky flowids}
const _protoMap = {} // nDPI discovered proto map { proto => Array or related flowids}

function _getAttribute (obj, field) {
  if (Object.hasOwn(obj, field)) {
    return obj[field]
  } else {
    return ''
  }
}

export default {

  parsePacket: function (pkt) {
    // logger.info('[parser/parsePacket] [' + pkt.packet_event_name + ']')
    // Let's update the counter for this kink of PACKET event...
    //
    // _c => event name
    const _c = pkt.packet_event_name
    if (_counters.packets.hasOwnProperty(_c)) {
      _counters.packets[_c]++
    } else {
      _counters.packets[_c] = 1
    }
  },

  parseFlow: function (f) {
    const _lastSeen = Math.max(f.flow_src_last_pkt_time, f.flow_dst_last_pkt_time)

    const times = sprintf('[%d msec]', _lastSeen - f.flow_first_seen)

    logger.debug('[parser/parseFlow] ' + f.flow_id + ' - ' + times + ' - ' + f.flow_event_name)

    // let's ensure that an entry for our flow, exists
    if (typeof (_flowMap[f.flow_id]) === 'undefined') {
      _flowMap[f.flow_id] = {
        'stateTimeline': [], // array holding "flow_event_name"s of related flow JSONs
        'stateTimelineTS': [], // array holding timestamp (millisend) related to arrival time of related flow JSONs
        'firstSeen': f.flow_first_seen,
        'lastSrcSeen': f.flow_src_last_pkt_time,
        'lastDstSeen': f.flow_dst_last_pkt_time,
        'evts': []
      }
    }

    // let's add the flow to thepopulate the flowMap
    _flowMap[f.flow_id].flow_id = f.flow_id
    _flowMap[f.flow_id].stateTimeline.push(f.flow_event_name)
    _flowMap[f.flow_id].stateTimelineTS.push(f.arrivalTime)
    _flowMap[f.flow_id].evts.push(f)
    _flowMap[f.flow_id].lastSrcSeen = f.flow_src_last_pkt_time
    _flowMap[f.flow_id].lastDstSeen = f.flow_dst_last_pkt_time

    // Let's update the counter for this kink of flow event...
    //
    // _c => event name
    const _c = f.flow_event_name
    if (_counters.flows.hasOwnProperty(_c)) {
      _counters.flows[_c]++
    } else {
      _counters.flows[_c] = 1
    }
    if (Object.hasOwn(f, 'ndpi')) {
      _ctrDetected++
      logger.debug('[parser/parseFlow] ' + f.flow_id + ' - ' + _ctrDetected + ' ==>' + f.src_ip + ':' + f.src_port + ' => ' + f.dst_ip + ':' + f.dst_port + ' - ' + JSON.stringify(f.ndpi) + '<==')
    }

    if (f.flow_event_name === 'end' || f.flow_event_name === 'idle') {
      // let's calculate the flow duration (in seconds; rounded)
      const dt = Math.round((Math.max(_flowMap[f.flow_id].lastSrcSeen, _flowMap[f.flow_id].lastDstSeen) - _flowMap[f.flow_id].firstSeen) / 1000)

      // let's ensure we have numeric ports (0, for ICMP)
      if (typeof (f.dst_port) === 'undefined') { f.dst_port = 0 }
      if (typeof (f.src_port) === 'undefined') { f.src_port = 0 }

      try {
        logger.debug(sprintf('[parser/parseFlow] Got [%s] event_name for flow [%d]: [%s:%d]=>[%s:%d] (%s) [%s] [%d]',
          f.flow_event_name,
          f.flow_id,
          f.src_ip,
          f.src_port,
          f.dst_ip,
          f.dst_port,
          f.l4_proto,
          _flowMap[f.flow_id].stateTimeline.join('|'),
          dt))

        if (_completedFlowMap.hasOwnProperty(f.flow_id)) {
          logger.error('[parser/parseFlow] end-ed flow [' + f.flow_id + '] already presente in _completedFlowMap!')
          return
        }

        // let's enrich flow data
        this.enrichData(_flowMap[f.flow_id])

        // let's move the flow data to the completed/terminated map
        _completedFlowMap[f.flow_id] = _flowMap[f.flow_id]
        delete _flowMap[f.flow_id]

        // Let's PUSH the new completed-flow to the injestor...
        injestor.processFlow(_completedFlowMap[f.flow_id])
      } catch (e) {
        logger.error(`[parser/parseFlow] error: [${e}]`)
        logger.error('[parser/parseFlow] ==>' + JSON.stringify(f) + '<==')
      }
    }
  },

  getFlowMap: function () {
    return _flowMap
  },

  getFlowMapSize: function () {
    return Object.keys(_flowMap).length
  },

  getCompletedFlowMap: function () {
    return _completedFlowMap
  },

  getRiskFlowMap: function () {
    return _riskFlowMap
  },

  getProtoMap: function () {
    return _protoMap
  },

  // empty the completed flow map (...mostly 'cause user just "dumped" it to file...)
  resetCompletedFlowMap: function () {
    _completedFlowMap = {}
  },

  getCompletedFlowMapSize: function () {
    return Object.keys(_completedFlowMap).length
  },

  getCounters: function () {
    return _counters
  },

  enrichData: async function (f) {
    // aggiungo 'duration'
    f.srcDuration = f.lastSrcSeen - f.firstSeen
    f.dstDuration = f.lastDstSeen - f.firstSeen

    // aggiungo 'num_evts'
    f.num_evts = f.stateTimeline.length

    // default ndpi_totalRisk to 0
    f.ndpi_totalRisk = 0

    // let's see if it's a "complete" flow (aka: with both "new" and "end|idle" events)
    let partial = false
    if (f.stateTimeline[0] !== 'new') { partial = true }
    if ((f.stateTimeline[f.stateTimeline.length - 1] !== 'end') &&
        (f.stateTimeline[f.stateTimeline.length - 1] !== 'idle')) {
      partial = true
    }
    f.is_partial = partial

    // array to track last two values, to check deltas and other...
    const valueStore = []

    for (let idx = 0; idx < f.evts.length; idx++) {
      const e = f.evts[idx]

      const _curValues = {
        'thread_ts': e.thread_ts_usec,
        'delta_ts': 0,
        'src_last_pkt_time': e.flow_src_last_pkt_time,
        'dst_last_pkt_time': e.flow_dst_last_pkt_time,
        'src_packets_processed': e.flow_src_packets_processed,
        'dst_packets_processed': e.flow_dst_packets_processed,
        'src_tot_l4_payload_len': e.flow_src_tot_l4_payload_len,
        'dst_tot_l4_payload_len': e.flow_dst_tot_l4_payload_len,
        'src_traffic_density': 0,
        'dst_traffic_density': 0,
        'src_timespan': 0,
        'dst_timespan': 0
      }
      // Payload Per Packet (AVG) - Src & Dst
      if (e.flow_src_packets_processed > 0) {
        _curValues.ppp_src = Math.round((e.flow_src_tot_l4_payload_len / e.flow_src_packets_processed) * 100) / 100
      }
      if (e.flow_dst_packets_processed > 0) {
        _curValues.ppp_dst = Math.round((e.flow_dst_tot_l4_payload_len / e.flow_dst_packets_processed) * 100) / 100
      }

      // populate valuestore array, to later calculate various attributes
      let _deltaSrcTs = 0
      let _deltaDstTs = 0
      let _deltaSrcTraffic = 0
      let _deltaDstTraffic = 0

      if (valueStore.length > 0) {
        const prevIndex = valueStore.length - 1
        const prevItem = valueStore[prevIndex]
        _curValues.delta_ts = e.thread_ts_usec - prevItem.thread_ts
        // logger.info(sprintf("[%d - %s - %d]", prevIndex, JSON.stringify(prevItem), _curValues.delta_ts))

        _deltaSrcTs = e.flow_src_last_pkt_time - prevItem.src_last_pkt_time
        _deltaSrcTraffic = e.flow_src_tot_l4_payload_len - prevItem.src_tot_l4_payload_len

        _deltaDstTs = e.flow_dst_last_pkt_time - prevItem.dst_last_pkt_time
        _deltaDstTraffic = e.flow_dst_tot_l4_payload_len - prevItem.dst_tot_l4_payload_len
      }

      if (_deltaSrcTs > 0) {
        _curValues.src_timespan = _deltaSrcTs
        _curValues.src_traffic_density = Math.round((_deltaSrcTraffic / _deltaSrcTs) * 100) / 100
      }
      if (_deltaDstTs > 0) {
        _curValues.dst_timespan = _deltaDstTs
        _curValues.dst_traffic_density = Math.round((_deltaDstTraffic / _deltaDstTs) * 100) / 100
      }

      // add new values to value store, and keep its length up to 2 (last, and previous to last)
      valueStore.push(_curValues)

      // recupero un po' di dati relativi al flusso, nella sua interezza,
      // disponibili e fine vita (idle o end)
      if (e.flow_event_name === 'idle' || e.flow_event_name === 'end') {
        f.src_min_l4_payload_len = e.flow_src_min_l4_payload_len
        f.src_max_l4_payload_len = e.flow_src_max_l4_payload_len
        f.dst_min_l4_payload_len = e.flow_dst_min_l4_payload_len
        f.dst_max_l4_payload_len = e.flow_dst_max_l4_payload_len
        f.src_tot_l4_payload_len = e.flow_src_tot_l4_payload_len
        f.dst_tot_l4_payload_len = e.flow_dst_tot_l4_payload_len

        f.src_num_packets = e.flow_src_packets_processed
        f.dst_num_packets = e.flow_dst_packets_processed

        f.l4_proto = e.l4_proto
        f.src_ip = e.src_ip
        f.src_ip_country = geoResolver.fetchCountry(e.src_ip)
        f.src_ip_asn = geoResolver.fetchASN(e.src_ip)

        f.dst_ip = e.dst_ip
        f.dst_ip_country = geoResolver.fetchCountry(e.dst_ip)
        f.dst_ip_asn = geoResolver.fetchASN(e.dst_ip)

        f.src_port = e.src_port
        f.dst_port = e.dst_port

        // TODO: this could be a perfect place to MAP "remote scanner" (to be filtered at firewall level)
        // if tot_l4_payload is zero, than it's a simple "probe" (it's a part of a port-scan)
        f.is_empty_probe = false
        if (f.src_tot_l4_payload_len + f.dst_tot_l4_payload_len === 0) {
          f.is_empty_probe = true
        }
      }

      // if "flow event" includes 'ndpi' object, let's extract some of its content
      // Note: we're looping over events, so only the last "ndpi" structure will be taken
      //       into account, as it's overwritten over the loop
      if (Object.hasOwn(e, 'ndpi')) {
        f.ndpi = e.ndpi
        f.ndpi_proto = _getAttribute(e.ndpi, 'proto')
        f.ndpi_category = _getAttribute(e.ndpi, 'category')
        f.ndpi_hostname = _getAttribute(e.ndpi, 'hostname')
      }
    }

    //
    //
    // update _protoMap to add reference to THIS risk
    //
    if (!_protoMap.hasOwnProperty(f.ndpi_proto)) {
      // it's a "new" risk. Let's create the empty array (...of riskIds)
      _protoMap[f.ndpi_proto] = []
      _counters.protos[f.ndpi_proto] = 0
      logger.info('[parser/parseFlow] New proto to be tracked: [' + f.ndpi_proto + ']')
    }
    logger.info('[parser/parseFlow] Adding flow: [' + f.flow_id + '] to proto: [' + f.ndpi_proto + ']')
    _protoMap[f.ndpi_proto].unshift(f.flow_id)
    _counters.protos[f.ndpi_proto]++

    // ensure max 100 flows kept in memory
    // TODO: this should be parametrized
    if (_protoMap[f.ndpi_proto].length > 100) {
      _protoMap[f.ndpi_proto].pop()
    }
    // ^^^^^^^^^^^^ Done ^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    //
    //
    // let's calculate the total risk score (ndpi.flow_risk.*.risk_score.total)
    let totalRisk = 0
    if (typeof (f.ndpi) !== 'undefined' && typeof (f.ndpi.flow_risk) !== 'undefined') {
      for (const k in f.ndpi.flow_risk) {
        // calculate/update total risk score
        const lScore = f.ndpi.flow_risk[k].risk_score.total
        totalRisk += lScore
        logger.info(sprintf('[parser/enrichData] Flow:[%d] - Found risk [%s] with total score [%d]', f.flow_id, k, lScore))

        // update counters for this specific risk
        // k is the "stanza" containing k's risk details
        const riskName = f.ndpi.flow_risk[k].risk
        if (_counters.risks.hasOwnProperty(riskName)) {
          _counters.risks[riskName]++

          // update _riskFlowMap to add reference to THIS risk
          if (!_riskFlowMap.hasOwnProperty(riskName)) {
            // it's a "new" risk. Let's create the empty array (...of riskIds)
            _riskFlowMap[riskName] = []
            logger.info('[parser/enrichData] New risk to be tracked: [' + riskName + '] _riskFlowMap!')
          } else {
            logger.info('[parser/enrichData] Adding flow: [' + f.flow_id + '] to risk: [' + riskName + ']')
            _riskFlowMap[riskName].unshift(f.flow_id)

            // ensure max 100 flows kept in memory
            // TODO: this should be parametrized
            if (_riskFlowMap[riskName].length > 100) {
              _riskFlowMap[riskName].pop()
            }
          }
        } else {
          logger.info('[parser/enrichData] New risk to count: [' + riskName + ']')
          // it's a new kind of risk. Need to update counters and riskmap
          _riskFlowMap[riskName] = []
          _riskFlowMap[riskName].unshift(f.flow_id)
          _counters.risks[riskName] = 1
        }
      }
      f.ndpi_totalRisk = totalRisk
    }
    // ^^^^^^^^^^^^^^ Done ^^^^^^^^^^^^^^^^^^

    let m1 = 0; let stdDev1 = 0; let m2 = 0; let stdDev2 = 0; const numVals = valueStore.length
    if (valueStore.length > 0) {
      // calculate the average and stddev src & dst traffic_densities
      m1 = valueStore.reduce((tmp, e) => tmp + e.src_traffic_density, valueStore[0].src_traffic_density) / numVals
      stdDev1 = Math.sqrt(valueStore.map((e) => Math.pow(e.src_traffic_density - m1, 2)).reduce((a, b) => a + b) / m1)

      m2 = valueStore.reduce((tmp, e) => tmp + e.dst_traffic_density, valueStore[0].dst_traffic_density) / numVals
      stdDev2 = Math.sqrt(valueStore.map((e) => Math.pow(e.dst_traffic_density - m2, 2)).reduce((a, b) => a + b) / m2)
    }

    // Let's store our metrics within the same flow
    f.stat_num_evts = numVals

    f.src_traffic_density_avg = m1
    f.src_traffic_density_stdev = stdDev1 || 0

    f.dst_traffic_density_avg = m2
    f.dst_traffic_density_stdev = stdDev2 || 0
  }

}
