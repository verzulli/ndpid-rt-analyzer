import winston from 'winston'

const myFormat = winston.format.printf(
  ({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`
  }
)

const logWithTSPrefix = new winston.transports.Console({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp({ format: 'DD-MM-YYYY HH:mm:ss.SSS' }),
    winston.format.label({ label: 'nDPId-rt-analyzer' }),
    myFormat
  )
})

const logger = winston.createLogger({
  format: myFormat,
  transports: [
    logWithTSPrefix
  ]
})

export default logger
