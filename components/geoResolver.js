import logger from './logger'
import { Reader } from '@maxmind/geoip2-node'
import path from 'path'
import { contains } from 'cidr-tools'

let _geoCountryReader = null
let _geoASNReader = null
let _moduleIsReady = false

function _isRFC1918 (ip) {
  return contains([
    '192.168.0.0/16',
    '172.16.0.0/12',
    '10.0.0.0/8'
  ], ip)
}

export default {

  init: async function () {
    // let's instantiate the two GeoIP2 readers
    logger.debug('[geoResolver/init] Initializing MaxMind GeoIP2 archives...')
    try {
      _geoCountryReader = await Reader.open(path.join(__dirname, '../GEOIP_DB/GeoLite2-Country.mmdb'))
      _geoASNReader = await Reader.open(path.join(__dirname, '/../GEOIP_DB/GeoLite2-ASN.mmdb'))

      // if we arrive here, then MaxMind support is ready and active!
      _moduleIsReady = true
    } catch (e) {
      logger.error('[geoResolver/init] Error initializing MaxMind GeoIP2 archives: [' + e + ']')
      _moduleIsReady = false
    }
  },

  fetchASN: (ip) => {
    logger.debug('[geoResolver/fetchASN] Resolving ASN for:[' + ip + ']')

    const res = {
      'number': 0,
      'name': 'Not available',
      'network': '0.0.0.0/0'
    }

    // if module is not ready (missing DB?), return null data
    if (!_moduleIsReady) { return res }

    try {
      if (_isRFC1918(ip)) {
        res.name = 'RFC 1918'
        return res
      }

      // TODO: some caching should be implemented!
      const data = _geoASNReader.asn(ip)
      res.number = data.autonomousSystemNumber
      res.name = data.autonomousSystemOrganization
      res.network = data.network
      logger.debug('[geoResolver/fetchASN] ASN for [' + ip + '] => [' + res.number + '|' + res.name + '|' + res.network + ']')
      return res
    } catch (e) {
      logger.error('[geoResolver/fetchASN] Error resolving ASN [' + ip + '] => [' + e + ']')
    }
  },

  fetchCountry: (ip) => {
    logger.debug('[geoResolver/fetchCountry] Resolving Country for :[' + ip + ']')

    const res = {
      countryCode: 'XX',
      countryName: 'Not Available'
    }

    // if module is not ready (missing DB?), return null data
    if (!_moduleIsReady) { return res }

    try {
      if (_isRFC1918(ip)) {
        res.countryName = 'local net'
        return res
      }

      // TODO: some caching should be implemented!
      const data = _geoCountryReader.country(ip)
      res.countryCode = data.country.isoCode
      res.countryName = data.country.names.en
      logger.debug('[geoResolver/fetchCountry] Country for [' + ip + '] => [' + res.countryCode + '|' + res.countryName + ']')
      return res
    } catch (e) {
      logger.error('[geoResolver/fetchCountry] Error resolving Country for [' + ip + '] => [' + e + ']')
    }
  }

}
