import logger from './logger'
import receiver from './receiver'
import parser from './parser'
import { sprintf } from 'sprintf-js'
import fs from 'node:fs'

function _uptimeString () {
  const startedOn = receiver.getStartedOn()

  logger.info(`[cliHandlers/_uptimeString] Got [${startedOn}]`)
  const now = Date.now()
  const startedObj = new Date(startedOn)
  const startedStr = startedObj.toString()

  const deltaS = Math.round((now - startedOn) / 1000)
  const str = Math.round(deltaS / 60)

  return `started on: ${startedStr} - uptime: ${deltaS} seconds [${str} minutes]`
}

export default {
  commandMap: [
    {
      'command': 'help',
      'regexp': /help/,
      'handler': 'showHelp',
      'description': 'Show help/usage'
    },
    {
      'command': 'show version',
      'regexp': /show version/,
      'handler': 'showVersion',
      'description': 'Show version number'
    },
    {
      'command': 'loglevel [debug|info|error]',
      'regexp': /^loglevel (?:debug|info|error)$/,
      'handler': 'loglevel',
      'description': 'Set LogLevel to debug, info or error only messages [...in the application server output]'
    },
    {
      'command': 'jsoncapture [on|off]',
      'regexp': /^jsoncapture (?:on|off)$/,
      'handler': 'jsoncapture',
      'description': 'Enable/Disable the capture of incoming JSON to /tmp/raw_json.stream'
    },
    {
      'command': 'start udp server',
      'regexp': /start udp server/,
      'handler': 'startUDPServer',
      'description': 'Start the UDP server to receive remote nDPId JSON stream'
    },
    {
      'command': 'connect <ip> <port>',
      'regexp': /^connect (?:\d+\.\d+\.\d+\.\d+) (\d+)$/,
      'handler': 'connectTCP',
      'description': 'Connect to remote nDPIsrvd, at <host> <port>'
    },
    {
      'command': 'stop receiver',
      'regexp': /stop receiver/,
      'handler': 'stopReceiver',
      'description': 'Stop the UDP server _AND_ TCP client [if active]'
    },
    {
      'command': 'show counters',
      'regexp': /show counter/,
      'handler': 'showCounters',
      'description': 'Show global `receiver` and `parser` counters'
    },
    {
      'command': 'show flow map',
      'regexp': /show flow map/,
      'handler': 'showFlowMap',
      'description': 'Show global flow-map'
    },
    {
      'command': 'show risk map',
      'regexp': /show risk map/,
      'handler': 'showRiskMap',
      'description': 'Show flow-ids related to specific nDPI risks'
    },
    {
      'command': 'show proto map',
      'regexp': /show proto map/,
      'handler': 'showProtoMap',
      'description': 'Show flow-ids related to specific nDPI proto'
    },
    {
      'command': 'show completed flows',
      'regexp': /show completed flows/,
      'handler': 'showCompletedFlowMap',
      'description': 'Show COMPLETED flow-map'
    },
    {
      'command': 'dump completed flows',
      'regexp': /dump completed flows/,
      'handler': 'dumpCompletedFlowMap',
      'description': 'dump COMPLETED flow-map in a .json file'
    },
    {
      'command': 'dump flow <flowid>',
      'regexp': /dump flow \d+/,
      'handler': 'dumpFlow',
      'description': 'dump full data related to flow <flowid>'
    }

  ],

  showHelp (socket) {
    logger.debug('[cliHandler/showHelp] show help')
    socket.write('\n')
    this.commandMap.forEach((handler) => {
      socket.write(sprintf('%25s: %s\n', handler.command, handler.description))
    })
    socket.write(sprintf('%25s: %s\n', 'exit | quit', 'exit the console, leaving everything running'))
    socket.write('\n')
  },

  showVersion (socket) {
    logger.debug('[cliHandler/showVersion] show version')
    socket.write('nDPId-rt-analyzer v. <some>')
    socket.write('(c) 2022 - Damiano Verzulli - damiano@verzulli.it')
    socket.write('Released under the A-GPL terms of License')
  },

  // this is called directly from engineConsole, as a "default"
  unknownCommand (socket) {
    logger.debug('[cliHandler/CLI/unknownCommand] ...')
    socket.write('Unknown command....')
  },

  // this is called directly from cliConsoleServer, as a "default"
  ambiguousCommand (socket) {
    logger.debug('[cliHandler/ambiguousCommand] ...')
    socket.write('Ambiguous command! Please restrict it....')
  },

  loglevel (socket, command) {
    const _p = command.match(/loglevel (.*)/)
    const _flag = _p[1]
    if (_flag === 'debug') {
      socket.write('Enabling \'debug\' log level...')
      logger.transports[0].level = 'debug'
    } else if (_flag === 'info') {
      socket.write('Enabling \'info\' log level...')
      logger.transports[0].level = 'info'
    } else if (_flag === 'error') {
      socket.write('Enabling \'error\' log level...')
      logger.transports[0].level = 'error'
    }
  },

  jsoncapture (socket, command) {
    const _p = command.match(/jsoncapture (.*)/)
    const _flag = _p[1]
    if (_flag === 'on') {
      socket.write('Adding JSONs to /tmp/raw_json.stream ...')
      receiver.enableJsonDumper()
    } else if (_flag === 'off') {
      socket.write('Stop adding data...')
      receiver.disableJsonDumper()
    }
  },

  startUDPServer () {
    // let's read the UDP port, from .env file
    const UDP_PORT = process.env.UDP_PORT || 9191

    logger.info('[cliHandlers] Starting UDP receiver on port [' + UDP_PORT + ']')
    receiver.initUDP(UDP_PORT)
  },

  connectTCP (socket, command) {
    const _p = command.match(/^connect (\d+\.\d+\.\d+\.\d+) (\d+)$/)
    const _host = _p[1]
    const _port = _p[2]
    logger.info('[cliHandlers/connectTCP] Connecting to [' + _host + ':' + _port + ']')
    receiver.initTCP(_host, _port)
  },

  stopReceiver () {
    logger.info('[cliHandlers] Stopping UDP _AND_ TCP receiver (if connected)')
    receiver.terminate()
  },

  showCounters (socket) {
    logger.debug('[cliHandler/showCounters] ...')
    const aCounters = receiver.getCounters()
    const pCounters = parser.getCounters()

    const msg1 = JSON.stringify(aCounters, null, 2)
    const msg2 = JSON.stringify(pCounters, null, 2)

    socket.write('=> main `receiver` counters:\n----------\n')
    socket.write(msg1)
    socket.write('\n----------\n')
    socket.write('=> specific `parser` counters:\n----------\n')
    socket.write(msg2)
    socket.write('\n----------\n')
    socket.write('=> FLOW TABLE SIZES:\n')
    socket.write(sprintf('=> Current Global table size...: [%7d] flows\n', parser.getFlowMapSize()))
    socket.write(sprintf('=> Current Completed table size: [%7d] flows\n', parser.getCompletedFlowMapSize()))
    socket.write('\n----------\n')
    socket.write(_uptimeString())
  },

  showFlowMap (socket) {
    logger.debug('[cliHandler/showFlowMap] ...')

    socket.write('=> Dumping global flow map:\n')

    const flowMap = parser.getFlowMap()

    const allFlowIDs = Object.keys(flowMap).sort((a, b) => { return ((Math.max(flowMap[a].lastSrcSeen, flowMap[a].lastSrcSeen) - flowMap[a].firstSeen) - (Math.max(flowMap[b].lastSrcSeen, flowMap[b].lastSrcSeen) - flowMap[b].firstSeen)) })
    allFlowIDs.forEach(flow => {
      const msg = sprintf('[%d] (%d)=> %s',
        flow,
        Math.max(flowMap[flow].lastSrcSeen, flowMap[flow].lastDstSeen) - flowMap[flow].firstSeen,
        flowMap[flow].stateTimeline.join('|'))

      socket.write(msg + '\n')
    })

    socket.write('\n')
    socket.write('total number of tracked flows: [' + allFlowIDs.length + ']\n\n')

    socket.write(_uptimeString())
  },

  showRiskMap (socket) {
    logger.debug('[cliHandler/showRiskMap] ...')

    socket.write('=> Dumping risk flow map:\n')

    const riskMap = parser.getRiskFlowMap()
    const flowMap = parser.getCompletedFlowMap()

    for (const r in riskMap) {
      let msg = sprintf('- Risk: [%s]\n', r)

      const riskyIDs = riskMap[r]
      riskyIDs.forEach(id => {
        const thisFlow = flowMap[id]
        msg += sprintf('-> id: [%d] => Src:[%s:%s] Dst:[%s:%s]\n',
          id,
          thisFlow.src_ip, thisFlow.src_port,
          thisFlow.dst_ip, thisFlow.dst_port
        )
      })
      socket.write(msg + '\n')
    }
    socket.write('Dump completed!\n')

    socket.write(_uptimeString())
  },

  showProtoMap (socket) {
    logger.debug('[cliHandler/showProtoMap] ...')

    socket.write('=> Dumping PROTO map:\n')

    const protoMap = parser.getProtoMap()
    const flowMap = parser.getCompletedFlowMap()

    for (const r in protoMap) {
      let msg = sprintf('- Proto: [%s]\n', r)

      const protoIDs = protoMap[r]
      protoIDs.forEach(id => {
        const thisFlow = flowMap[id]
        msg += sprintf('-> id: [%d] => Src:[%s:%s] Dst:[%s:%s]\n',
          id,
          thisFlow.src_ip, thisFlow.src_port,
          thisFlow.dst_ip, thisFlow.dst_port
        )
      })
      socket.write(msg + '\n')
    }
    socket.write('Dump completed!\n')

    socket.write(_uptimeString())
  },

  showCompletedFlowMap (socket) {
    logger.debug('[cliHandler/showCompletedFlowMap] ...')

    socket.write('=> Dumping COMPLETED flow map:\n')

    const flowMap = parser.getCompletedFlowMap()

    // const allFlowIDs = Object.keys(flowMap).sort((a, b) => { return ((flowMap[a].lastSeen - flowMap[a].firstSeen) - (flowMap[b].lastSeen - flowMap[b].firstSeen)) })
    const allFlowIDs = Object.keys(flowMap)
    allFlowIDs.forEach(flow => {
      const msg = sprintf('[%d] (%d)=> %s',
        flow,
        Math.max(flowMap[flow].lastSrcSeen, flowMap[flow].lastDstSeen) - flowMap[flow].firstSeen,
        flowMap[flow].stateTimeline.join('|'))

      socket.write(msg + '\n')
    })

    socket.write('\n')
    socket.write('total number of completed flows: [' + allFlowIDs.length + ']\n\n')

    socket.write(_uptimeString())
  },

  dumpCompletedFlowMap (socket) {
    logger.debug('[cliHandler/dumpCompletedFlowMap] ...')

    const _folder = process.env.STORE_PATH || '/tmp'
    const _suffix = Date.now()
    const dumpFile = sprintf('%s/ndpid_rt_analizer_dump_%d.json', _folder, _suffix)

    socket.write('=> Dumping COMPLETED flow map in [' + dumpFile + ']:\n')

    try {
      const fd = fs.openSync(dumpFile, 'a')
      const flowMap = parser.getCompletedFlowMap()

      let counter = 0

      const allFlowIDs = Object.keys(flowMap)
      allFlowIDs.forEach(flow => {
        fs.appendFileSync(fd, JSON.stringify(flowMap[flow]) + '\n', 'utf8')
        counter++

        if (!(counter % 1000)) {
          socket.write('wrote [' + counter + '] json...\n')
        }
      })
      fs.closeSync(fd)

      // purge the main map, as we just dumped it!
      parser.resetCompletedFlowMap()

      socket.write('\n')
      socket.write('Done! Written [' + counter + '] flows to file [' + dumpFile + ']\n\n')
    } catch (e) {
      logger.error('[cliHandler/dumpCompletedFlowMap] Error creating dump file: [' + e + ']')
    }
  },

  dumpFlow (socket, command) {
    logger.debug('[cliHandler/dumpFlow] command: [' + command + ']')

    const _p = command.match(/dump flow (\d+)/)
    const _flowID = _p[1]

    socket.write('=> Dumping FLOW data for flow-id: [' + _flowID + ']\n')

    const flowCMap = parser.getCompletedFlowMap()
    const flowPMap = parser.getFlowMap()

    if (flowCMap.hasOwnProperty(_flowID)) {
      socket.write('- found COMPLETED flow data\n')
      socket.write('---------------------\n' + JSON.stringify(flowCMap[_flowID]) + '\n-----------------------------\n')
    } else if (flowPMap.hasOwnProperty(_flowID)) {
      socket.write('- found ON-GOING flow data\n')
      socket.write('---------------------\n' + JSON.stringify(flowPMap[_flowID]) + '\n-----------------------------\n')
    } else {
      socket.write('- found NO-DATA flow\n')
    }

    socket.write(_uptimeString())
  }

}
