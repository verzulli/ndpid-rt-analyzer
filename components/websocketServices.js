import logger from './logger'
import WebSocket, { WebSocketServer } from 'ws'
import receiver from './receiver'
import parser from './parser'

let _WSserver = null
let _interval1 = null
let _interval2 = null

function _wsClientsCheckAlive () {
  this.isAlive = true
}

export default {

  start: function () {
    const wsServerPort = process.env.WSSERVER_PORT || 3300

    _WSserver = new WebSocketServer({ port: wsServerPort })

    logger.info('[websocketServices/start] Starting WS server on port [' + wsServerPort + ']')

    _WSserver.on('connection', function connection (ws, req) {
      ws.isAlive = true
      const clientIpAddress = req.socket.remoteAddress

      // TODO: add check for proxyied connections
      // const ip = req.headers['x-forwarded-for'].split(',')[0].trim();
      logger.info('[websocketServices/ws_hdl_connection] Got a new WS connection from [' + clientIpAddress + ']')
      ws.on('message', function message (data) {
        logger.info('[websocketServices/ws_hdl_connection_msg] Got new MSG from a WS client: [' + data + ']')
      })
      // ws.send('something')
      ws.on('pong', _wsClientsCheckAlive)
    })

    _WSserver.on('close', function close () {
      // clearInterval(_interval1)
      clearInterval(_interval2)
    })

    // check client alive, every 30 seconds
    _interval1 = setInterval(function ping () {
      logger.info('[websocketServices/_checkInterval] Checking client liveness')
      _WSserver.clients.forEach(function each (ws) {
        if (ws.isAlive === false) return ws.terminate()
        ws.isAlive = false
        ws.ping()
      })
    }, 30000)

    // send counter updates every 5 seconds
    _interval2 = setInterval(() => { this.sendCounters() }, 5000)
  },

  terminate: async () => {
    logger.info('[websocketServices/terminate] Destroying _interval1 and _interval2 timers...')
    clearInterval(_interval1)
    clearInterval(_interval2)

    logger.info('[websocketServices/terminate] Close opened web-sockets...')
    await _WSserver.clients.forEach(async (wsClient) => {
      logger.info('[websocketServices/terminate] Terminating client... the SOFT way...')
      wsClient.close()

      process.nextTick(() => {
        // readyState => https://developer.mozilla.org/en-US/docs/Web/API/WebSocket/readyState
        if (wsClient.readyState === 1 || wsClient.readyState === 2) {
          // Socket still hangs, hard close
          logger.info('[websocketServices/terminate] Terminating client... the HARD way...')
          wsClient.terminate()
        }
      })
    })

    // this .close() call is _MANDATORY_, in order to gracefully terminate the 
    // NodeJS server process (event-loop)
    _WSserver.close()
  },

  sendWSmessage: (data) => {
    // logger.debug('[websocketServices/sendWSmessage] Devo SPAMmare ai client WS il messaggio: [' + JSON.stringify(data) + ']')
    const msg = {
      'class': 'events',
      'content': data
    }
    try {
      _WSserver.clients.forEach(function each (client) {
        if (client !== _WSserver && client.readyState === WebSocket.OPEN) {
          logger.debug('[websocketServices/sendWSmessage] Invio ad un client ==> ' + JSON.stringify(msg) + ' <==')
          client.send(JSON.stringify(msg))
        }
      })
    } catch (e) {
      logger.error('[websocketServices/sendWSmessage] error [' + e + ']')
    }
  },

  sendCounters: function () {
    logger.debug('[websocketServices/sendCounters] Sending counters')

    const aCounters = receiver.getCounters()
    const pCounters = parser.getCounters()

    const msg = {
      'class': 'counters',
      'content': {
        'receiver': aCounters,
        'parser': pCounters
      }
    }

    try {
      _WSserver.clients.forEach(function each (client) {
        if (client !== _WSserver && client.readyState === WebSocket.OPEN) {
          logger.debug('[websocketServices/sendCounters] Invio ad un client ==> ' + JSON.stringify(msg) + ' <==')
          client.send(JSON.stringify(msg))
        }
      })
    } catch (e) {
      logger.error('[websocketServices/sendCounters] error [' + e + ']')
    }
  }
}
