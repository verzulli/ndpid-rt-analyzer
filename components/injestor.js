import logger from './logger'
import { sprintf } from 'sprintf-js'
import { Kafka } from 'kafkajs'
import websocketServer from './websocketServices'

// local vars to interact with Kafka
let _producer = null
let _kafkaOutTopic = null
let _kafkaEnrichTopic = null

// give a flow_event_name, return true if related JSON contains the "ndpi" object
function _haveNDPIdata (name) {
  // list of names containing "ndpi" objects
  const listName = ['guessed', 'detected', 'detection-update', 'not-detected']
  return listName.find((n) => n === name)
}

export default {

  init: async function () {
    // let's connect to OUTPUT TOPIC
    const kafkaHost = process.env.KAFKA_HOST
    _kafkaOutTopic = process.env.KAFKA_OUTPUT_TOPIC
    _kafkaEnrichTopic = process.env.KAFKA_ENRICH_TOPIC

    logger.debug('[injestor/init] Pushing ndpid terminated flows to TOPIC [' + _kafkaOutTopic + ']')

    try {
      // connect to kafka (an additional connection, distinct from main consumer)
      const kafka = new Kafka({
        clientId: 'ndpid-rt-analyzer',
        brokers: [kafkaHost]
      })

      _producer = kafka.producer()
      // await _producer.connect()

      logger.debug('[injestor/init] Kafka producer is ready!')
    } catch (e) {
      logger.error('Error: [' + e + ']')
    }
  },

  processFlow: function (f) {
    logger.debug('[injstor/processFlow] Spammo ==> ' + JSON.stringify(f) + ' <==')
    websocketServer.sendWSmessage(f)
  }

}
