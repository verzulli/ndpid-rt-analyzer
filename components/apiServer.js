import express from 'express'
import cors from 'cors'
import logger from './logger'
import webService from './webServices'

// ************** API ROUTES IMPLEMENTED *******************
function appRoutes (router) {
  router.route('/fetchcounters')
    .get(webService.fetchCounters)
  router.route('/starttcpreceiver')
    .post(webService.tcpConnect)
  router.route('/stopreceiver')
    .get(webService.stopReceiver)
  router.route('/statusreceiver')
    .get(webService.statusReceiver)
  router.route('/fetchriskmap')
    .get(webService.fetchRiskMap)
  router.route('/fetchprotomap')
    .get(webService.fetchProtoMap)

  return router
}
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

// TCP socket handler
let _APIserver = null

export default {

  start: () => {
    // define listening TCP port: 3001, unless defined in config
    const apiServerPort = process.env.APISERVER_PORT || 8080

    logger.info('[APISERVER] Starting API SERVER on port [' + apiServerPort + ']')

    const app = express()
    const router = express.Router()
    app.use(cors())

    // to properly process POST requests
    app.use(express.json())
    app.use(express.urlencoded({
      extended: true
    }))

    app.options('*', cors()) // per CORS e pre-flight
    app.use('/', appRoutes(router))

    _APIserver = app.listen(apiServerPort, function () {
      logger.info('[APISERVER] Listening on port [' + apiServerPort + ']')
    })
  },

  terminate: () => {
    logger.info('[APISERVER/terminate] Closing the _APIserver socket...')
    _APIserver.close()
  }
}
